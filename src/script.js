
// Теоритичні питання:
// 1. В чому відмінність між setInterval та setTimeout?
//setInterval - подія/дія буде відбуватися багато разів з вказаною періодичністю
//setTimeout - подія/дія відбудеться 1 раз через вказаний час 

// 2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно 
//через той проміжок часу, який ви вказали? - Теоретично так.

// 3. Як припинити виконання функції, яка була запланована для виклику з 
//використанням setTimeout та setInterval?
// clearTimeout , clearInterval





// Практичне завдання 1:


// -Створіть HTML-файл із кнопкою та елементом div.

// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div 
//через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

const changeBtn = document.getElementById('changeBtn');

function changeContent() {
    const contentDiv = document.getElementById('contentDiv')
    contentDiv.textContent = "Changes haven been alredy made"
    console.log( "Changes haven been alredy made");
};

changeBtn.addEventListener('click',() => {
    setTimeout(changeContent, 3000)
} );


// Практичне завдання 2:


// Реалізуйте таймер зворотного відліку, використовуючи setInterval. 
//При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
// Після досягнення 1 змініть текст на "Зворотній відлік завершено".

function startCountdown () {
    let startTime = 10;

    const timerElement = document.getElementById('timer');
    timerElement.textContent = startTime;

    const intervalId = setInterval(() => {
        startTime--;
        timerElement.textContent = startTime;

        if(startTime <= 0) {
            clearInterval(intervalId);
            setTimeout(() => {
                console.log('Your time is out');
                timerElement.textContent = 'Countdown is over';
        }, 0);
    }
    }, 1000);
};
const startTimerBtn = document.getElementById("startTimer");
startTimerBtn.addEventListener('click', startCountdown);